import env from '../services/env';

env.restrictToServer();

export default {
  secretKey: process.env.SECRET_KEY,
  dbURL: process.env.MONGODB_URI,
  bcryptSaltRounds: parseInt(process.env.BCRYPT_SALT_ROUNDS, 10),
  tmdbApiBase: process.env.TMDB_URI,
  tmdbApiKey: process.env.TMDB_API_KEY
};
