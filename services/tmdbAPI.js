import axios from 'axios';
import config from '../config/server';

const axiosInstance = axios.create({
  baseURL: config.tmdbApiBase,
  headers: { 'Content-Type': 'application/json' }
});

export default axiosInstance;
