import Joi from 'joi';

export const signInSchema = Joi.object().keys({
  password: Joi.string()
    .label('Password')
    .min(3)
    .max(30)
    .required(),
  email: Joi.string()
    .label('Email')
    .min(3)
    .max(30)
    .required()
});

export const signUpSchema = Joi.object().keys({
  password: Joi.string()
    .label('Password')
    .min(3)
    .max(30)
    .required(),
  email: Joi.string()
    .label('Email')
    .min(3)
    .max(30)
    .required()
});

export const getMoviesSchema = Joi.object().keys({
  title: Joi.string()
    .label('Title')
    .required()
});

export const favoriteMovieSchema = Joi.object().keys({
  isNew: Joi.bool().required(),
  id: Joi.number().required()
});

export const validate = (schema, isBody) => (req, res, next) => {
  const validationResult = isBody
    ? schema.validate(req.body)
    : schema.validate(req.query);
  if (validationResult.error) {
    const {
      error: { details }
    } = validationResult;
    const errorsList = details.map(error => error.message);

    return res.send({ errors: errorsList });
  }
  return next();
};
