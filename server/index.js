import express from 'express';
import compression from 'compression';
import cors from 'cors';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import env from '../services/env';
import mongoose from './mongoose';
import controllers from './controllers/';
import swaggerDocument from './swagger.json';

const port = process.env.PORT || 80;

const app = express();
if (env.isProd()) {
  app.use(compression());
}
app.use(cors());
app.use(bodyParser.json());
app.use('/api', controllers);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
mongoose.connect();
app.listen(port, () => {
  console.log(`API server is listening on port ${port}`);
});
