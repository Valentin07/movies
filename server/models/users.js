const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const userSchema = new Schema(
  {
    email: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    favorites: [{ type: String }]
  },
  { emitIndexErrors: true }
);

userSchema.statics.signIn = function signIn(username, password) {
  return this.find({ username, password }).exec();
};

export default mongoose.model('User', userSchema);
