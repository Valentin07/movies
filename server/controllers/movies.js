import express from 'express';
import config from '../../config/server';
import logger from '../../services/serverLogger';
import tmdbAPI from '../../services/tmdbAPI';
import { authRequired } from '../../services/authToken';
import {
  getMoviesSchema,
  favoriteMovieSchema,
  validate
} from '../../services/validation';
import User from '../models/users';

const router = new express.Router();

router.get(
  '/search',
  authRequired,
  validate(getMoviesSchema, false),
  (req, res) => {
    const { title } = req.query;
    tmdbAPI
      .get('/search/movie', {
        params: { api_key: config.tmdbApiKey, query: title }
      })
      .then(searchResponse => {
        const { data } = searchResponse;
        res.send({ success: true, data });
      })
      .catch(error => {
        res.send({ error: error.message });
        logger.error(error.message);
      });
  }
);

router.put(
  '/favorite',
  authRequired,
  validate(favoriteMovieSchema, true),
  (req, res) => {
    const { id: userId } = req.user;
    const { id: tmdbId, isNew } = req.body;

    User.updateOne(
      { _id: userId },
      isNew
        ? { $push: { favorites: tmdbId } }
        : { $pull: { favorites: tmdbId } }
    )
      .then(() =>
        res.send({
          success: true,
          message: isNew
            ? 'Successfully added to favorites.'
            : 'Successfully removed from favorites.'
        })
      )
      .catch(error => {
        res.send({ error: error.message });
        logger.error(error.message);
      });
  }
);

export default router;
