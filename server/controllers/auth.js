import express from 'express';
import bcrypt from 'bcrypt';
import { isEmpty, get } from 'lodash';
import logger from '../../services/serverLogger';
import User from '../models/users';
import { genAuthToken } from '../../services/authToken';
import {
  signInSchema,
  signUpSchema,
  validate
} from '../../services/validation';
import config from '../../config/server';

const router = new express.Router();

router.post('/signup', validate(signUpSchema, true), (req, res) => {
  const userData = {
    email: req.body.email,
    password: req.body.password
  };
  bcrypt
    .hash(userData.password, config.bcryptSaltRounds)
    .then(hashedPassword => {
      userData.password = hashedPassword;

      User.create(userData)
        .then(() =>
          res.send({
            success: true,
            message: 'Account created successfully.'
          })
        )
        .catch(error => {
          res.send({ error: error.message });
        });
    });
});

router.post('/signin', validate(signInSchema, true), (req, res) => {
  const { email, password, isExpiring } = req.body;

  User.findOne({ email })
    .then(userResponse => {
      if (isEmpty(userResponse)) {
        throw new Error('Wrong username or password');
      }
      if (bcrypt.compareSync(password, userResponse.password)) {
        const id = get(userResponse, ['_id']);
        genAuthToken({ id, ...userResponse }, isExpiring).then(authToken =>
          res.send({ authToken })
        );
      } else {
        throw new Error('Wrong username or password');
      }
    })
    .catch(error => {
      res.send({ error: error.message });
      logger.error(error.message);
    });
});

export default router;
